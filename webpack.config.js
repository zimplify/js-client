module.exports = {
  entry: './app/main.js',
  output: {
    filename: 'zimplify.js'
  },
  module: {
    rules: [{ test: /\.js$/, use: 'babel-loader' }]
  },
  devServer: {
    port: 3000
  }
};
