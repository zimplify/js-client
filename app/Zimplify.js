import Connection from './Connection';
import ip from 'ip';
import jwt from 'jsonwebtoken';

/**
 * this is the generic Zimplify application connector include expiry handling
 */
export default class Zimplify {
  /**
   * creating our instance
   * @param {String} rules
   * @param {String} handshake
   * @param {Array} tokens the list of tokens allow in the connection
   */
  constructor(rules, handshake, tokens) {
    this.connection = new Connection(rules);
    this.device = null;
    this.handshake = handshake;
    this.tokendef = tokens;
    this.tokens = {};
  }

  /**
   * check if the data headers are part of what we need
   * @param {Object} headers
   * @return {void}
   */
  assign(headers) {
    headers.forEach((f, v) => {
      if (this.tokens.include(f)) this.tokens[f] = v;
    });
  }

  /**
   * creating a connection to the application
   * @return {Boolean}
   */
  connect(user, secret) {
    this.connection
      .send('connect', { token: this.generate(user, secret) })
      .then(response => {
        this.assign(response.headers);
        return true;
      })
      .catch(error => {
        console.log(error.body);
        return false;
      });
  }

  /**
   * reconnect to the application and renew the token
   * @return {void}
   */
  reconnect() {
    this.connection.send('reconnect', {}, this.tokens).then(response => {
      if (response.code == 208) {
        this.assign(response.headers);
        return response.body;
      } else return response.body;
    });
  }

  /**
   * requesting a command from application
   * @param {*} method the method we are requesting
   * @param {*} data the data values we are submitting
   * @return {Object}
   */
  request(method, data) {
    this.connection
      .send(method, data, this.tokens)
      .then(response => {
        if (response.code == 208) this.assign(response.headers);
        return response;
      })
      .catch(error => {
        // catch our expiry code
        if (error.code == 504) {
          // once we reconnect, try againn
          if (this.reconnect()) {
            return this.request(method, data);
          } else throw new Error('Failed to connect to application.');
        } else throw new Error(error.body);
      });
  }

  /**
   * creating our token for the device
   * @param {String} user the user to login to the service
   * @param {String} secret the secret that user use to access their profile
   * @return {String}
   */
  generate(user, secret) {
    return JSON.stringify(
      jwt.sign(
        { address: ip.address(), target: user, secret: secret },
        this.handshake
      )
    );
  }
}
