import axios from 'axios';
import DOMParser from 'xmldom';
import xpath from 'xpath';

// some constants we are trying to do
const REST_DELETE = 'delete';
const REST_GET = 'get';
const REST_PATCH = 'patch';
const REST_POST = 'post';
const REST_PUT = 'put';
const XPATH_GET_COMMAND = "//command[@name='<c>']";

/**
 *
 */
export default class Connection {
  /**
   * creating a new connection instance
   * @param {String} rules the ruleset we will use to identify the methods
   * @return {void}
   */
  constructor(rules) {
    this.rules = new DOMParser().parseFromString(rules);
  }

  /**
   * execute a HTTP connection base on the method received
   * @param {String} method the method we are calling
   * @param {Object} data the data we include in the package
   * @param {Object} token the header tokens that we include as part of the sending
   * @return {Promise}
   */
  send(method, data, tokens) {
    // let's get the command we are targeting
    let commands = xpath.select(
      XPATH_GET_COMMAND.replace('<c>', method),
      this.rules
    );

    // if we have the command then let's do it
    if (commands.length > 0) {
      let command = commands[0];
      let result = null;

      // we are going to find out the method and things.
      let method = command.hasAttribute('method')
        ? command.getAttribute('method')
        : null;
      let endpoint = command.hasAttribute('endpoint')
        ? this.update(command.getAttribute('endpoint'), data)
        : null;

      // now we need to make sure we fiter out the data
      if (method && endpoint) {
        // now we have to make sure the command data is clean
        if (!this.validate(command, data))
          throw new Error('Data provided failed to meet requirement.');

        // now let's play by our method
        switch (method) {
          case REST_DELETE:
            result = new Promise((resolve, reject) => {
              axios
                .post(endpint, data, { headers: tokens })
                .then(response => {
                  resolve(response);
                })
                .catch(error => {
                  reject(error);
                });
            });
          case REST_GET:
            result = new Promise(function(resolve, reject) {
              axios
                .get(endpoint, { headers: tokens })
                .then(response => {
                  resolve(response);
                })
                .catch(error => {
                  reject(error);
                });
            });
            break;
          case REST_PATCH:
            result = new Promise((resolve, reject) => {
              axios
                .post(endpint, data, { headers: tokens })
                .then(response => {
                  resolve(response);
                })
                .catch(error => {
                  reject(error);
                });
            });
          case REST_POST:
            result = new Promise((resolve, reject) => {
              axios
                .post(endpint, data, { headers: tokens })
                .then(response => {
                  resolve(response);
                })
                .catch(error => {
                  reject(error);
                });
            });
          case REST_PUT:
            result = new Promise((resolve, reject) => {
              axios
                .post(endpint, data, { headers: tokens })
                .then(response => {
                  resolve(response);
                })
                .catch(error => {
                  reject(error);
                });
            });
            break;
        }

        // now return our promise
        return result;
      } else throw new Error('Command defined is invalid.');
    } else throw new Error('Unable to locate command ' + method + '.');
  }

  /**
   * updating the endpoint for us to get the right endpoint
   * @param {String} endpoint the endpoint we are targeting with data to sub
   * @param {Object} data the data we are loading up
   * @return {String}
   */
  update(endpoint, data) {
    let result = endpoint;

    // now match in the endpoint
    endpoint.match(/{.?}/).forEach(match => {
      let tag = match.replace('{', '').replace('}', '');
      if (data.hasObjectProperty(tag)) result.replace(match, data[tag]);
    });

    // return the result
    return result;
  }

  /**
   * check our data and see if we have all we need
   * @param {Node} structure
   * @param {Object} data
   * @return {Boolean}
   */
  validate(structure, data) {
    let result = true;

    // now check our structure
    structure.childNodes.forEach(item => {
      let field = item.hasAttribute('name') ? item.getAttribute('name') : null;
      let type = item.hasAttribute('type') ? item.getAttribute('type') : null;

      // make sure we have field of type
      if (field && type) {
        if (data.hasObjectProperty(field)) {
          let check = true;
          let value = data[field];
          switch (type) {
            case 'date':
              check = value instanceof Date;
              break;
            case 'text':
              check = typeof value === 'string' || value instanceof String;
              break;
            case 'decimal':
            case 'number':
              check = typeof value === 'number' || isFinite(value);
              break;
            case 'yesno':
              check = typeof value === 'boolean';
              break;
          }

          // if we failed, then break out
          if (!check) {
            result = check;
            return;
          }
          // if we do not have the data, then let's check the requirement
        } else if (item.hasAttribute('required')) {
          result = false;
          return;
        }
      } else {
        result = false;
        return;
      }
    });

    // now return
    return result;
  }
}
